package com.data.ofac.mappers.profilerelation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "ProfileRelationship", namespace = "http://www.un.org/sanctions/1.0")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProfileRelationship {

  @XmlAttribute(name = "ID")
  private int relationshipID=0;

  @XmlAttribute(name = "From-ProfileID")
  private int fromId=0;

  @XmlAttribute(name = "To-ProfileID")
  private int toId=0;

  @XmlAttribute(name = "RelationTypeID")
  private int typeId=0;

  @XmlAttribute(name = "RelationQualityID")
  private int qualityId=0;

  @XmlAttribute(name = "Former")
  private boolean former=false;

  @XmlAttribute(name = "SanctionsEntryID")
  private int sanctionId=0;

}
