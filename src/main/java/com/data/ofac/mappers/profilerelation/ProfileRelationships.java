package com.data.ofac.mappers.profilerelation;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "ProfileRelationships",namespace = "http://www.un.org/sanctions/1.0")
public class ProfileRelationships {

  @XmlElement(name = "ProfileRelationship", namespace = "http://www.un.org/sanctions/1.0")
  private List<ProfileRelationship> profileRelationships;

  public List<ProfileRelationship> getProfileRelationships(){return profileRelationships;}

}
