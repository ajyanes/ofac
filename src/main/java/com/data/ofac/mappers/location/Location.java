package com.data.ofac.mappers.location;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "Location", namespace = "http://www.un.org/sanctions/1.0")
@XmlAccessorType(XmlAccessType.FIELD)
public class Location implements Serializable {

  @XmlAttribute(name = "ID")
  private int locationId=0;

  @XmlElementRef(name = "LocationAreaCode",type = AreaCodeLoc.class)
  private AreaCodeLoc areaCode;

  @XmlElementRef(name = "LocationCountry",type = CountryLoc.class)
  private CountryLoc country;

  @XmlElementRef(name = "LocationPart",type = LocationPart.class)
  private List<LocationPart> locationPart;

}
