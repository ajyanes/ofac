package com.data.ofac.mappers.location;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "Locations",namespace = "http://www.un.org/sanctions/1.0")
public class Locations {

  @XmlElement(name = "Location", namespace = "http://www.un.org/sanctions/1.0")
  private List<Location> location=null;

  public List<Location> getLocations(){return location;}

}
