package com.data.ofac.mappers.regdocument;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "LocationCountry", namespace = "http://www.un.org/sanctions/1.0")
@XmlAccessorType(XmlAccessType.FIELD)
public class RegDocument {

  @XmlAttribute(name = "ID")
  private int regDocumentId=0;

  @XmlAttribute(name = "IDRegDocTypeID")
  private int docType=0;

  @XmlAttribute(name = "IdentityID")
  private int identity=0;

  @XmlAttribute(name = "IssuedBy-CountryID")
  private int IssuedBy=0;

  @XmlAttribute(name = "ValidityID")
  private int validity=0;

  @XmlElement(name = "IDRegistrationNo", namespace = "http://www.un.org/sanctions/1.0")
  private String registrationNumber;

  @XmlElementRef(name = "NameRef",type = NameReference.class)
  private NameReference nameReference;

}
