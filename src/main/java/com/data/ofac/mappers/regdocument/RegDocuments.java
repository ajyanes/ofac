package com.data.ofac.mappers.regdocument;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "IDRegDocuments",namespace = "http://www.un.org/sanctions/1.0")
public class RegDocuments {

  @XmlElement(name = "IDRegDocument", namespace = "http://www.un.org/sanctions/1.0")
  private List<RegDocument> regDocuments;

  public List<RegDocument> getRegDocuments(){return regDocuments;}

}
