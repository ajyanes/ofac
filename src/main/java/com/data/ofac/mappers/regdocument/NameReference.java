package com.data.ofac.mappers.regdocument;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "DocumentedNameReference", namespace = "http://www.un.org/sanctions/1.0")
@XmlAccessorType(XmlAccessType.FIELD)
public class NameReference {

  @XmlAttribute(name = "DocumentedNameID")
  private int nameId=0;

}
