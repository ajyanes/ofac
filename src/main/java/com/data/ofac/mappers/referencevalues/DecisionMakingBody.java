package com.data.ofac.mappers.referencevalues;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "DecisionMakingBody", namespace = "http://www.un.org/sanctions/1.0")
@XmlAccessorType(XmlAccessType.FIELD)
public class DecisionMakingBody {

  @XmlAttribute(name = "ID")
  private int decisionMakingId = 0;

  @XmlAttribute(name = "OrganisationID")
  private int organisationId = 0;

  @XmlValue
  private String decisionMakingName="";

}
