package com.data.ofac.mappers.referencevalues;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "AreaCode", namespace = "http://www.un.org/sanctions/1.0")
@XmlAccessorType(XmlAccessType.FIELD)
public class AreaCode {

  @XmlAttribute(name = "ID")
  private int areaCodeId=0;

  @XmlAttribute(name = "CountryID")
  private int countryId=0;

  @XmlAttribute(name = "Description")
  private String description="";

  @XmlAttribute(name = "AreaCodeTypeID")
  private int areaCodeType=0;

  @XmlValue
  private String areaCodeName="";

}
