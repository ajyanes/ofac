package com.data.ofac.mappers.referencevalues;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "ValidityValues", namespace = "http://www.un.org/sanctions/1.0")
@XmlAccessorType(XmlAccessType.FIELD)
public class ValidityValues {

  @XmlElementRef(name = "Validity", type = Validity.class)
  private List<Validity> validities;

}
