package com.data.ofac.mappers.referencevalues;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "LegalBasis", namespace = "http://www.un.org/sanctions/1.0")
@XmlAccessorType(XmlAccessType.FIELD)
public class LegalBasis {

  @XmlAttribute(name = "ID")
  private int legalBasisId = 0;

  @XmlAttribute(name = "LegalBasisShortRef")
  private String legalBasisShortRef = "";

  @XmlAttribute(name = "LegalBasisTypeID")
  private int legalBasisTypeID = 0;

  @XmlAttribute(name = "SanctionsProgramID")
  private int sanctionsProgramID = 0;

  @XmlValue
  private String legalBasisName="";

}
