package com.data.ofac.mappers.referencevalues;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "ReferenceValueSets", namespace = "http://www.un.org/sanctions/1.0")
@XmlAccessorType(XmlAccessType.FIELD)
public class ReferenceValueSet {

  @XmlElementRef(name = "AliasTypeValues",type = AliasTypeValues.class)
  private AliasTypeValues aliasTypeValues;

  @XmlElementRef(name = "AreaCodeValues",type = AreaCodeValues.class)
  private AreaCodeValues areaCodeValues;

  @XmlElementRef(name = "CalendarTypeValues",type = CalendarTypeValues.class)
  private CalendarTypeValues calendarTypeValues;

  @XmlElementRef(name = "CountryValues",type = CountryValues.class)
  private CountryValues countryValues;

  @XmlElementRef(name = "CountryRelevanceValues",type = CountryRelevanceValues.class)
  private CountryRelevanceValues countryRelevanceValues;

  @XmlElementRef(name = "DecisionMakingBodyValues",type = DecisionMakingBodyValues.class)
  private DecisionMakingBodyValues decisionMakingBodyValues;

  @XmlElementRef(name = "DetailReferenceValues",type = DetailReferenceValues.class)
  private DetailReferenceValues detailReferenceValues;

  @XmlElementRef(name = "DetailTypeValues",type = DetailTypeValues.class)
  private DetailTypeValues detailTypeValues;

  @XmlElementRef(name = "DocNameStatusValues",type = DocNameStatusValues.class)
  private DocNameStatusValues docNameStatusValues;

  @XmlElementRef(name = "EntryEventTypeValues",type = EntryEventTypeValues.class)
  private EntryEventTypeValues entryEventTypeValues;

  @XmlElementRef(name = "FeatureTypeValues",type = FeatureTypeValues.class)
  private FeatureTypeValues featureTypeValues;

  @XmlElementRef(name = "FeatureTypeGroupValues",type = FeatureTypeGroupValues.class)
  private FeatureTypeGroupValues featureTypeGroupValues;

  @XmlElementRef(name = "IDRegDocDateTypeValues",type = IDRegDocDateTypeValues.class)
  private IDRegDocDateTypeValues idRegDocDateTypeValues;

  @XmlElementRef(name = "IDRegDocTypeValues",type = IDRegDocTypeValues.class)
  private IDRegDocTypeValues idRegDocTypeValues;

  @XmlElementRef(name = "IdentityFeatureLinkTypeValues",type = IdentityFeatureLinkTypeValues.class)
  private IdentityFeatureLinkTypeValues identityFeatureLinkTypeValues;

  @XmlElementRef(name = "LegalBasisValues",type = LegalBasisValues.class)
  private LegalBasisValues legalBasisValues;

  @XmlElementRef(name = "LegalBasisTypeValues",type = LegalBasisTypeValues.class)
  private LegalBasisTypeValues legalBasisTypeValues;

  @XmlElementRef(name = "ListValues",type = ListValues.class)
  private ListValues listValues;

  @XmlElementRef(name = "LocPartTypeValues",type = LocPartTypeValues.class)
  private LocPartTypeValues locPartTypeValues;

  @XmlElementRef(name = "LocPartValueStatusValues",type = LocPartValueStatusValues.class)
  private LocPartValueStatusValues locPartValueStatusValues;

  @XmlElementRef(name = "LocPartValueTypeValues",type = LocPartValueTypeValues.class)
  private LocPartValueTypeValues locPartValueTypeValues;

  @XmlElementRef(name = "NamePartTypeValues",type = NamePartTypeValues.class)
  private NamePartTypeValues namePartTypeValues;

  @XmlElementRef(name = "OrganisationValues",type = OrganisationValues.class)
  private OrganisationValues organisationValues;

  @XmlElementRef(name = "PartySubTypeValues",type = PartySubTypeValues.class)
  private PartySubTypeValues partySubTypeValues;

  @XmlElementRef(name = "PartyTypeValues",type = PartyTypeValues.class)
  private PartyTypeValues partyTypeValues;

  @XmlElementRef(name = "RelationQualityValues",type = RelationQualityValues.class)
  private RelationQualityValues relationQualityValues;

  @XmlElementRef(name = "RelationTypeValues",type = RelationTypeValues.class)
  private RelationTypeValues relationTypeValues;

  @XmlElementRef(name = "ReliabilityValues",type = ReliabilityValues.class)
  private ReliabilityValues reliabilityValues;

  @XmlElementRef(name = "SanctionsProgramValues",type = SanctionsProgramValues.class)
  private SanctionsProgramValues sanctionsProgramValues;

  @XmlElementRef(name = "SanctionsTypeValues",type = SanctionsTypeValues.class)
  private SanctionsTypeValues sanctionsTypeValues;

  @XmlElementRef(name = "ScriptValues",type = ScriptValues.class)
  private ScriptValues scriptValues;

  @XmlElementRef(name = "ScriptStatusValues",type = ScriptStatusValues.class)
  private ScriptStatusValues scriptStatusValues;

  @XmlElementRef(name = "SubsidiaryBodyValues",type = SubsidiaryBodyValues.class)
  private SubsidiaryBodyValues subsidiaryBodyValues;

  @XmlElementRef(name = "ValidityValues",type = ValidityValues.class)
  private ValidityValues validityValues;

}
