package com.data.ofac.mappers;

import com.data.ofac.mappers.distinctparties.DistinctParties;
import com.data.ofac.mappers.location.Locations;
import com.data.ofac.mappers.referencevalues.ReferenceValueSet;
import com.data.ofac.mappers.regdocument.RegDocuments;
import com.data.ofac.mappers.profilerelation.ProfileRelationships;
import com.data.ofac.mappers.sanctionentry.SanctionsEntries;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
@XmlRootElement(name = "Sanctions", namespace="http://www.un.org/sanctions/1.0")
@XmlAccessorType(XmlAccessType.FIELD)
public class Sanctions {

  @XmlElementRef(name = "DateOfIssue",type = DateOfIssue.class)
  private DateOfIssue dateOfIssue;

  @XmlElementRef(name = "ReferenceValueSets",type = ReferenceValueSet.class)
  private ReferenceValueSet referenceValueSet;

  @XmlElementRef(name = "Locations",type = Locations.class)
  private Locations locations;

  @XmlElementRef(name = "RegDocuments",type = RegDocuments.class)
  private RegDocuments regDocuments;

  @XmlElementRef(name = "ProfileRelationships",type = ProfileRelationships.class)
  private ProfileRelationships profileRelationships;

  @XmlElementRef(name = "SanctionsEntries",type = SanctionsEntries.class)
  private SanctionsEntries sanctionsEntries;

  @XmlElementRef(name = "DistinctParties",type = DistinctParties.class)
  private DistinctParties distinctParties;

}
