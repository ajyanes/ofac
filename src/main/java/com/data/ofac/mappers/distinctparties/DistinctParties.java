package com.data.ofac.mappers.distinctparties;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "DistinctParties",namespace = "http://www.un.org/sanctions/1.0")
public class DistinctParties {

  @XmlElement(name = "DistinctParty", namespace = "http://www.un.org/sanctions/1.0")
  private List<DistinctParty> distinctPartyList;

  public List<DistinctParty> getDistinctPartyList(){return distinctPartyList;}

}
