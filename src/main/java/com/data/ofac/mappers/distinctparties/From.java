package com.data.ofac.mappers.distinctparties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "From", namespace = "http://www.un.org/sanctions/1.0")
@XmlAccessorType(XmlAccessType.FIELD)
public class From {

  @XmlElement(name = "Year",namespace = "http://www.un.org/sanctions/1.0")
  private String year="";
  @XmlElement(name = "Month",namespace = "http://www.un.org/sanctions/1.0")
  private String month="";
  @XmlElement(name = "Day",namespace = "http://www.un.org/sanctions/1.0")
  private String day="";

}
