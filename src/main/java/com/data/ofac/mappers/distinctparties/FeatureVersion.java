package com.data.ofac.mappers.distinctparties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "FeatureVersion", namespace = "http://www.un.org/sanctions/1.0")
@XmlAccessorType(XmlAccessType.FIELD)
public class FeatureVersion {

  @XmlAttribute(name = "ID")
  private int featureVersionId=0;

  @XmlAttribute(name = "ReliabilityID")
  private int reliabilityId=0;

  @XmlElementRef(name = "DatePeriod",type = DatePeriodDistinct.class)
  private DatePeriodDistinct datePeriod;

  @XmlElementRef(name = "VersionDetail",type = VersionDetail.class)
  private VersionDetail versionDetail;

  @XmlElementRef(name = "VersionLocation",type = VersionLocation.class)
  private VersionLocation versionLocation;

}
