package com.data.ofac.mappers.distinctparties;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "Identity", namespace = "http://www.un.org/sanctions/1.0")
@XmlAccessorType(XmlAccessType.FIELD)
public class Identity {

  @XmlAttribute(name = "ID")
  private int identityId=0;

  @XmlAttribute(name = "FixedRef")
  private int ref=0;

  @XmlAttribute(name = "Primary")
  private boolean primary=false;

  @XmlAttribute(name = "False")
  private boolean falseValue=false;

  @XmlElementRef(name = "Alias",type = Alias.class)
  private List<Alias> alias;

  @XmlElementRef(name = "NamePartGroups",type = NamePartGroups.class)
  private NamePartGroups namePartGroups;

}
