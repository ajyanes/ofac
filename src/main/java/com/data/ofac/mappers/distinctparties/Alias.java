package com.data.ofac.mappers.distinctparties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "Alias", namespace = "http://www.un.org/sanctions/1.0")
@XmlAccessorType(XmlAccessType.FIELD)
public class Alias {

  @XmlAttribute(name = "FixedRef")
  private int fixedRef=0;

  @XmlAttribute(name = "AliasTypeID")
  private int aliasType=0;

  @XmlAttribute(name = "Primary")
  private boolean primary=false;

  @XmlAttribute(name = "LowQuality")
  private boolean lowQuality=false;

  @XmlElementRef(name = "DocumentedName",type = DocumentedName.class)
  private DocumentedName documentedName;

}
