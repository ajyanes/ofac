package com.data.ofac.mappers.distinctparties;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "DocumentedName", namespace = "http://www.un.org/sanctions/1.0")
@XmlAccessorType(XmlAccessType.FIELD)
public class DocumentedName {

  @XmlAttribute(name = "ID")
  private int documentID=0;

  @XmlAttribute(name = "FixedRef")
  private int docFixedRef=0;

  @XmlAttribute(name = "DocNameStatusID")
  private int docNameStatus=0;

  @XmlElementRef(name = "DocumentedNamePart",type = DocumentedNamePart.class)
  private List<DocumentedNamePart> documentedNameParts;

}
