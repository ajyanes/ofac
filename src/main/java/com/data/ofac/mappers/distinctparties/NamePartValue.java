package com.data.ofac.mappers.distinctparties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "NamePartValue", namespace = "http://www.un.org/sanctions/1.0")
@XmlAccessorType(XmlAccessType.FIELD)
public class NamePartValue {

  @XmlAttribute(name = "NamePartGroupID")
  private int partGroup=0;

  @XmlAttribute(name = "ScriptID")
  private int script=0;

  @XmlAttribute(name = "ScriptStatusID")
  private int scriptStatus=0;

  @XmlAttribute(name = "Acronym")
  private boolean acronym=false;

  @XmlValue
  private String value;

}
