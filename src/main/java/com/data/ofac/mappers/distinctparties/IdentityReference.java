package com.data.ofac.mappers.distinctparties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "IdentityReference", namespace = "http://www.un.org/sanctions/1.0")
@XmlAccessorType(XmlAccessType.FIELD)
public class IdentityReference {

  @XmlAttribute(name = "IdentityID")
  private int idRefId=0;

  @XmlAttribute(name = "IdentityFeatureLinkTypeID")
  private int featureLinkType=0;

}
