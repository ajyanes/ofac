package com.data.ofac.mappers.distinctparties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "Feature", namespace = "http://www.un.org/sanctions/1.0")
@XmlAccessorType(XmlAccessType.FIELD)
public class FeatureParty {

  @XmlAttribute(name = "ID")
  private int featureId=0;

  @XmlElementRef(name = "FeatureVersion",type = FeatureVersion.class)
  private FeatureVersion featureVersion;

  @XmlElementRef(name = "IdentityReference",type = IdentityReference.class)
  private IdentityReference identityReference;

}
