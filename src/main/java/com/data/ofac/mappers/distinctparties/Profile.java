package com.data.ofac.mappers.distinctparties;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "Profile", namespace = "http://www.un.org/sanctions/1.0")
@XmlAccessorType(XmlAccessType.FIELD)
public class Profile {

  @XmlAttribute(name = "ID")
  private int profileId=0;

  @XmlAttribute(name = "PartySubTypeID")
  private int partySubType=0;

  @XmlElementRef(name = "Identity",type = Identity.class)
  private Identity identity;

  @XmlElementRef(name = "Feature",type = FeatureParty.class)
  private List<FeatureParty> features;

}
