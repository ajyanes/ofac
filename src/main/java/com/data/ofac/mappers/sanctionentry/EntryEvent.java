package com.data.ofac.mappers.sanctionentry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "EntryEvent", namespace = "http://www.un.org/sanctions/1.0")
@XmlAccessorType(XmlAccessType.FIELD)
public class EntryEvent {

  @XmlAttribute(name = "ID")
  private int entryId=0;

  @XmlAttribute(name = "EntryEventTypeID")
  private int eventType=0;

  @XmlAttribute(name = "LegalBasisID")
  private int legalBasis=0;

  @XmlElementRef(name = "EntryDate",type = EntryDate.class)
  private EntryDate entryDate;

}
