package com.data.ofac.mappers.sanctionentry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "DatePeriod", namespace = "http://www.un.org/sanctions/1.0")
@XmlAccessorType(XmlAccessType.FIELD)
public class DatePeriod {

  @XmlAttribute(name = "CalendarTypeID")
  private int calendarTypeID=0;

  @XmlAttribute(name = "YearFixed")
  private boolean yearFixed=false;

  @XmlAttribute(name = "MonthFixed")
  private boolean monthFixed=false;

  @XmlAttribute(name = "DayFixed")
  private boolean dayFixed=false;

}
