package com.data.ofac.mappers.sanctionentry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "SanctionsMeasure", namespace = "http://www.un.org/sanctions/1.0")
@XmlAccessorType(XmlAccessType.FIELD)
public class Measure {

  @XmlAttribute(name = "ID")
  private int measureId=0;

  @XmlAttribute(name = "SanctionsTypeID")
  private int typeId=0;

  @XmlElementRef(name = "DatePeriod",type = DatePeriod.class)
  private DatePeriod datePeriod;

  @XmlElement(name = "Comment",namespace = "http://www.un.org/sanctions/1.0")
  private String comment="";

}
