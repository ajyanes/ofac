package com.data.ofac.mappers.sanctionentry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "Date", namespace = "http://www.un.org/sanctions/1.0")
@XmlAccessorType(XmlAccessType.FIELD)
public class EntryDate {

  @XmlAttribute(name = "CalendarTypeID")
  private int calendarType=0;

  @XmlElement(name = "Year",namespace = "http://www.un.org/sanctions/1.0")
  private String year="";

  @XmlElement(name = "Month",namespace = "http://www.un.org/sanctions/1.0")
  private String month="";

  @XmlElement(name = "Day",namespace = "http://www.un.org/sanctions/1.0")
  private String day="";

}
