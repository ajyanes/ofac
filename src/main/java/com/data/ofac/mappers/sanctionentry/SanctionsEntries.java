package com.data.ofac.mappers.sanctionentry;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "SanctionsEntries",namespace = "http://www.un.org/sanctions/1.0")
public class SanctionsEntries {

  @XmlElement(name = "SanctionsEntry", namespace = "http://www.un.org/sanctions/1.0")
  private List<SanctionEntry> sanctionEntries;

  public List<SanctionEntry> getSanctionEntries(){return sanctionEntries;}

}
