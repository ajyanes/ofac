package com.data.ofac.mappers.sanctionentry;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "SanctionsEntry", namespace = "http://www.un.org/sanctions/1.0")
@XmlAccessorType(XmlAccessType.FIELD)
public class SanctionEntry {

  @XmlAttribute(name = "ID")
  private int entryId=0;

  @XmlAttribute(name = "ProfileID")
  private int profileId=0;

  @XmlAttribute(name = "ListID")
  private int ListId=0;

  @XmlElementRef(name = "EntryEvent",type = EntryEvent.class)
  private EntryEvent entryEvents;

  @XmlElementRef(name = "SanctiosMeasure",type = Measure.class)
  private List<Measure> measure;

}
