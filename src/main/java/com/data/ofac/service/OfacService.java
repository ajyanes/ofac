package com.data.ofac.service;

import java.io.IOException;

public interface OfacService {

  Integer validateUrl() throws IOException;
  void saveOfac();

}
