package com.data.ofac.service.impl;

import com.data.ofac.domain.AliasPersist;
import com.data.ofac.domain.DateOfIssuePersist;
import com.data.ofac.domain.DistinctPartyPersist;
import com.data.ofac.domain.DocumentedNamePersist;
import com.data.ofac.domain.EntryEventPersist;
import com.data.ofac.domain.IdentityPersist;
import com.data.ofac.domain.LocationPartPersist;
import com.data.ofac.domain.LocationPersist;
import com.data.ofac.domain.MasterNamePartGroupPersist;
import com.data.ofac.domain.MeasurePersist;
import com.data.ofac.domain.NamePartPersist;
import com.data.ofac.domain.ProfilePartyPersist;
import com.data.ofac.domain.ProfilePersist;
import com.data.ofac.domain.RegDocumentsPersist;
import com.data.ofac.domain.SanctionEntryPersist;
import com.data.ofac.domain.subdomain.AliasTypePersist;
import com.data.ofac.domain.subdomain.AreaCodePersist;
import com.data.ofac.domain.subdomain.CalendarPersist;
import com.data.ofac.domain.subdomain.CountryPersist;
import com.data.ofac.domain.subdomain.CountryRelPersist;
import com.data.ofac.domain.subdomain.DecisionPersist;
import com.data.ofac.domain.subdomain.DetailRefPersist;
import com.data.ofac.domain.subdomain.DetailTypePersist;
import com.data.ofac.domain.subdomain.DocNamePersist;
import com.data.ofac.domain.subdomain.EntryEventTypePersist;
import com.data.ofac.domain.subdomain.FeatureGroupPersist;
import com.data.ofac.domain.subdomain.FeatureTypePersist;
import com.data.ofac.domain.subdomain.IdentityFeaturePersist;
import com.data.ofac.domain.subdomain.LegalBasisPersist;
import com.data.ofac.domain.subdomain.LegalBasisTypePersist;
import com.data.ofac.domain.subdomain.ListPersist;
import com.data.ofac.domain.subdomain.LocPartPersist;
import com.data.ofac.domain.subdomain.LocPartValueStatusPersist;
import com.data.ofac.domain.subdomain.LocPartValueTypePersist;
import com.data.ofac.domain.subdomain.NamePartTypePersist;
import com.data.ofac.domain.subdomain.OrgPersist;
import com.data.ofac.domain.subdomain.PartySubtypePersist;
import com.data.ofac.domain.subdomain.PartyTypePersist;
import com.data.ofac.domain.subdomain.RegDocTypePersist;
import com.data.ofac.domain.subdomain.RelQualityPersist;
import com.data.ofac.domain.subdomain.RelTypePersist;
import com.data.ofac.domain.subdomain.ReliabilityPersist;
import com.data.ofac.domain.subdomain.SanctionProgPersist;
import com.data.ofac.domain.subdomain.SanctionTypePersist;
import com.data.ofac.domain.subdomain.ScriptPersist;
import com.data.ofac.domain.subdomain.ScriptStatusPersist;
import com.data.ofac.domain.subdomain.SubsidiaryPersist;
import com.data.ofac.domain.subdomain.ValidityPersist;
import com.data.ofac.mappers.DateOfIssue;
import com.data.ofac.mappers.Sanctions;
import com.data.ofac.mappers.distinctparties.Alias;
import com.data.ofac.mappers.distinctparties.DistinctParty;
import com.data.ofac.mappers.distinctparties.DocumentedName;
import com.data.ofac.mappers.distinctparties.DocumentedNamePart;
import com.data.ofac.mappers.distinctparties.Identity;
import com.data.ofac.mappers.distinctparties.MasterNamePartGroup;
import com.data.ofac.mappers.distinctparties.NamePartGroup;
import com.data.ofac.mappers.distinctparties.NamePartValue;
import com.data.ofac.mappers.distinctparties.Profile;
import com.data.ofac.mappers.location.Location;
import com.data.ofac.mappers.location.LocationPart;
import com.data.ofac.mappers.profilerelation.ProfileRelationship;
import com.data.ofac.mappers.referencevalues.AliasType;
import com.data.ofac.mappers.referencevalues.AreaCode;
import com.data.ofac.mappers.referencevalues.CalendarType;
import com.data.ofac.mappers.referencevalues.Country;
import com.data.ofac.mappers.referencevalues.CountryRelevance;
import com.data.ofac.mappers.referencevalues.DecisionMakingBody;
import com.data.ofac.mappers.referencevalues.DetailReference;
import com.data.ofac.mappers.referencevalues.DetailType;
import com.data.ofac.mappers.referencevalues.DocNameStatus;
import com.data.ofac.mappers.referencevalues.EntryEventType;
import com.data.ofac.mappers.referencevalues.FeatureType;
import com.data.ofac.mappers.referencevalues.FeatureTypeGroup;
import com.data.ofac.mappers.referencevalues.IDRegDocDateType;
import com.data.ofac.mappers.referencevalues.IDRegDocType;
import com.data.ofac.mappers.referencevalues.IdentityFeatureLinkType;
import com.data.ofac.mappers.referencevalues.LegalBasis;
import com.data.ofac.mappers.referencevalues.LegalBasisType;
import com.data.ofac.mappers.referencevalues.ListVal;
import com.data.ofac.mappers.referencevalues.LocPartType;
import com.data.ofac.mappers.referencevalues.LocPartValueStatus;
import com.data.ofac.mappers.referencevalues.LocPartValueType;
import com.data.ofac.mappers.referencevalues.NamePartType;
import com.data.ofac.mappers.referencevalues.Organisation;
import com.data.ofac.mappers.referencevalues.PartySubType;
import com.data.ofac.mappers.referencevalues.PartyType;
import com.data.ofac.mappers.referencevalues.RelationQuality;
import com.data.ofac.mappers.referencevalues.RelationType;
import com.data.ofac.mappers.referencevalues.Reliability;
import com.data.ofac.mappers.referencevalues.SanctionsProgram;
import com.data.ofac.mappers.referencevalues.SanctionsType;
import com.data.ofac.mappers.referencevalues.Script;
import com.data.ofac.mappers.referencevalues.ScriptStatus;
import com.data.ofac.mappers.referencevalues.SubsidiaryBody;
import com.data.ofac.mappers.referencevalues.Validity;
import com.data.ofac.mappers.regdocument.RegDocument;
import com.data.ofac.mappers.sanctionentry.EntryEvent;
import com.data.ofac.mappers.sanctionentry.Measure;
import com.data.ofac.mappers.sanctionentry.SanctionEntry;
import com.data.ofac.repository.AliasRepository;
import com.data.ofac.repository.DateOfIssueRepository;
import com.data.ofac.repository.DistinctPartyRepository;
import com.data.ofac.repository.DocumentedNameRepository;
import com.data.ofac.repository.EntryEventRepository;
import com.data.ofac.repository.IdentityRepository;
import com.data.ofac.repository.LocationPartRepository;
import com.data.ofac.repository.LocationRepository;
import com.data.ofac.repository.MasterNamePartGroupRepository;
import com.data.ofac.repository.MeasureRepository;
import com.data.ofac.repository.NamePartRepository;
import com.data.ofac.repository.ProfilePartyRepository;
import com.data.ofac.repository.ProfileRepository;
import com.data.ofac.repository.RegDocumentsRepository;
import com.data.ofac.repository.SanctionEntryRepository;
import com.data.ofac.repository.referencesrepository.AliasTypeRepository;
import com.data.ofac.repository.referencesrepository.AreaCodeRepository;
import com.data.ofac.repository.referencesrepository.CalendarRepository;
import com.data.ofac.repository.referencesrepository.CountryRelRepository;
import com.data.ofac.repository.referencesrepository.CountryRepository;
import com.data.ofac.repository.referencesrepository.DecisionRepository;
import com.data.ofac.repository.referencesrepository.DetailRefRepository;
import com.data.ofac.repository.referencesrepository.DetailTypeRepository;
import com.data.ofac.repository.referencesrepository.DocNameRepository;
import com.data.ofac.repository.referencesrepository.EntryEventTypeRepository;
import com.data.ofac.repository.referencesrepository.FeatureGroupRepository;
import com.data.ofac.repository.referencesrepository.FeatureTypeRepository;
import com.data.ofac.repository.referencesrepository.IdentityFeatureRepository;
import com.data.ofac.repository.referencesrepository.LegalBasisRepository;
import com.data.ofac.repository.referencesrepository.LegalBasisTypeRepository;
import com.data.ofac.repository.referencesrepository.ListRepository;
import com.data.ofac.repository.referencesrepository.LocPartRepository;
import com.data.ofac.repository.referencesrepository.LocPartValueStatusRepository;
import com.data.ofac.repository.referencesrepository.LocPartValueTypeRepository;
import com.data.ofac.repository.referencesrepository.NamePartTypeRepository;
import com.data.ofac.repository.referencesrepository.OrgRepository;
import com.data.ofac.repository.referencesrepository.PartySubtypeRepository;
import com.data.ofac.repository.referencesrepository.PartyTypeRepository;
import com.data.ofac.repository.referencesrepository.RegDocTypeRepository;
import com.data.ofac.repository.referencesrepository.RelQualityRepository;
import com.data.ofac.repository.referencesrepository.RelTypeRepository;
import com.data.ofac.repository.referencesrepository.ReliabilityRepository;
import com.data.ofac.repository.referencesrepository.SanctionProgRepository;
import com.data.ofac.repository.referencesrepository.SanctionTypeRepository;
import com.data.ofac.repository.referencesrepository.ScriptRepository;
import com.data.ofac.repository.referencesrepository.ScriptStatusRepository;
import com.data.ofac.repository.referencesrepository.SubsidiaryRepository;
import com.data.ofac.repository.referencesrepository.ValidityRepository;
import com.data.ofac.service.OfacService;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class OfacServiceImpl implements OfacService {

  @Value("${data.url}")
  private String url;

  @Autowired
  private DateOfIssueRepository dateOfIssueRepository;
  @Autowired
  private AliasTypeRepository aliasTypeRepository;
  @Autowired
  private AreaCodeRepository areaCodeRepository;
  @Autowired
  private CalendarRepository calendarRepository;
  @Autowired
  private CountryRepository countryRepository;
  @Autowired
  private CountryRelRepository countryRelRepository;
  @Autowired
  private DecisionRepository decisionRepository;
  @Autowired
  private DetailRefRepository detailRefRepository;
  @Autowired
  private DetailTypeRepository detailTypeRepository;
  @Autowired
  private DocNameRepository docNameRepository;
  @Autowired
  private EntryEventTypeRepository entryEventTypeRepository;
  @Autowired
  private FeatureTypeRepository featureTypeRepository;
  @Autowired
  private FeatureGroupRepository featureGroupRepository;
  @Autowired
  private RegDocTypeRepository regDocTypeRepository;
  @Autowired
  private IdentityFeatureRepository identityFeatureRepository;
  @Autowired
  private LegalBasisRepository legalBasisRepository;
  @Autowired
  private LegalBasisTypeRepository legalBasisTypeRepository;
  @Autowired
  private ListRepository listRepository;
  @Autowired
  private LocPartRepository locPartRepository;
  @Autowired
  private LocPartValueStatusRepository locPartValueStatusRepository;
  @Autowired
  private LocPartValueTypeRepository locPartValueTypeRepository;
  @Autowired
  private NamePartTypeRepository namePartTypeRepository;
  @Autowired
  private OrgRepository orgRepo;
  @Autowired
  private PartySubtypeRepository partySubtypeRepository;
  @Autowired
  private PartyTypeRepository partyTypeRepository;
  @Autowired
  private RelQualityRepository relQualityRepository;
  @Autowired
  private RelTypeRepository relTypeRepository;
  @Autowired
  private ReliabilityRepository reliabilityRepository;
  @Autowired
  private SanctionProgRepository sanctionProgRepository;
  @Autowired
  private SanctionTypeRepository sanctionTypeRepository;
  @Autowired
  private ScriptRepository scriptRepository;
  @Autowired
  private ScriptStatusRepository scriptStatusRepository;
  @Autowired
  private SubsidiaryRepository subsidiaryRepository;
  @Autowired
  private ValidityRepository validityRepository;
  @Autowired
  private ProfileRepository profileRepository;
  @Autowired
  private LocationPartRepository locationPartRepository;
  @Autowired
  private LocationRepository locationRepository;
  @Autowired
  private DistinctPartyRepository distinctPartyRepository;
  @Autowired
  private ProfilePartyRepository profilePartyRepository;
  @Autowired
  private IdentityRepository identityRepository;
  @Autowired
  private AliasRepository aliasRepository;
  @Autowired
  private DocumentedNameRepository documentedNameRepository;
  @Autowired
  private NamePartRepository namePartRepository;
  @Autowired
  private MasterNamePartGroupRepository masterNamePartGroupRepository;
  @Autowired
  private RegDocumentsRepository regDocumentsRepository;
  @Autowired
  private SanctionEntryRepository sanctionEntryRepository;
  @Autowired
  private EntryEventRepository entryEventRepository;
  @Autowired
  private MeasureRepository measureRepository;


  @Override
  public Integer validateUrl() throws IOException {
    URL route = new URL(url);
    HttpURLConnection huc = (HttpURLConnection) route.openConnection();

    return huc.getResponseCode();
  }

  @Override
  public void saveOfac() {
    getData();
  }

  public void getData(){
    try {
      JAXBContext context = JAXBContext.newInstance(Sanctions.class);
      Unmarshaller unmarshaller = context.createUnmarshaller();
      URL ur = new URL(url);
      Sanctions sanc = (Sanctions) unmarshaller.unmarshal(ur);
      System.out.println("Inicio de Guardado");
      saveDateIssue(sanc.getDateOfIssue());
      saveAliasType(sanc.getReferenceValueSet().getAliasTypeValues().getAliasTypes());
      saveAreaCode(sanc.getReferenceValueSet().getAreaCodeValues().getAreaCode());
      saveCalendar(sanc.getReferenceValueSet().getCalendarTypeValues().getCalendarType());
      saveCountry(sanc.getReferenceValueSet().getCountryValues().getCountries(),sanc.getReferenceValueSet().getCountryRelevanceValues()
          .getCountryRelevance());
      saveDecisionMaking(sanc.getReferenceValueSet().getDecisionMakingBodyValues()
          .getDecisionMakingBody());
      saveDetailRef(sanc.getReferenceValueSet().getDetailReferenceValues().getDetailReference());
      saveFeatureType(sanc.getReferenceValueSet().getFeatureTypeValues().getFeatureTypes());
      saveRegDocType(sanc.getReferenceValueSet().getIdRegDocTypeValues().getIdRegDocType());
      saveLegalBasis(sanc.getReferenceValueSet().getLegalBasisValues().getLegalBases());
      saveSanctionProg(sanc.getReferenceValueSet().getSanctionsProgramValues().getSanctionsPrograms());
      saveDetailType(sanc.getReferenceValueSet().getDetailTypeValues().getDetailType());
      saveDocNameStatus(sanc.getReferenceValueSet().getDocNameStatusValues().getDocNameStatuses());
      saveEntryEventType(sanc.getReferenceValueSet().getEntryEventTypeValues().getEntryEventType());
      saveFeatureGroup(sanc.getReferenceValueSet().getFeatureTypeGroupValues().getFeatureTypeGroup());
      saveIdentityFeature(sanc.getReferenceValueSet().getIdentityFeatureLinkTypeValues()
          .getIdentityFeatureLinkType());
      saveLegalBasisType(sanc.getReferenceValueSet().getLegalBasisTypeValues().getLegalBasisType());
      saveList(sanc.getReferenceValueSet().getListValues().getListVal());
      saveLocPartType(sanc.getReferenceValueSet().getLocPartTypeValues().getLocPartType());
      saveLocPartStatus(sanc.getReferenceValueSet().getLocPartValueStatusValues().getLocPartValueStatus());
      saveLocPartValType(sanc.getReferenceValueSet().getLocPartValueTypeValues().getLocPartValueType());
      saveNamePartType(sanc.getReferenceValueSet().getNamePartTypeValues().getNamePartType());
      saveOrg(sanc.getReferenceValueSet().getOrganisationValues().getOrganisation());
      savePartSubType(sanc.getReferenceValueSet().getPartySubTypeValues().getPartySubTypes());
      savePartyType(sanc.getReferenceValueSet().getPartyTypeValues().getPartyTypes());
      saveRelQuality(sanc.getReferenceValueSet().getRelationQualityValues().getRelationQualities());
      saveRelationType(sanc.getReferenceValueSet().getRelationTypeValues().getRelationTypes());
      saveReliability(sanc.getReferenceValueSet().getReliabilityValues().getReliabilities());
      saveSanctionType(sanc.getReferenceValueSet().getSanctionsTypeValues().getSanctionsTypes());
      saveScript(sanc.getReferenceValueSet().getScriptValues().getScripts());
      saveScriptStatus(sanc.getReferenceValueSet().getScriptStatusValues().getScriptStatus());
      saveSubsidiary(sanc.getReferenceValueSet().getSubsidiaryBodyValues().getSubsidiaryBody());
      saveValidity(sanc.getReferenceValueSet().getValidityValues().getValidities());

      saveProfileRelation(sanc.getProfileRelationships().getProfileRelationships());
      saveDistinctParty(sanc.getDistinctParties().getDistinctPartyList());
      saveRegDocuments(sanc.getRegDocuments().getRegDocuments());
      saveSanctionEntry(sanc.getSanctionsEntries().getSanctionEntries());

      saveLocation(sanc.getLocations().getLocations());


      System.out.println("Datos Guardados!!");
    } catch (JAXBException jaxb) {
      jaxb.printStackTrace();
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
  }

  private void saveDateIssue(DateOfIssue dateOfIssue){
    log.info("Guardando DateIssue");
    DateOfIssuePersist dateOfIssuePersist=DateOfIssuePersist.builder()
        .calendartype(dateOfIssue.getCalendarType())
        .year(dateOfIssue.getYear())
        .month(dateOfIssue.getMonth())
        .day(dateOfIssue.getDay())
        .build();

    dateOfIssueRepository.save(dateOfIssuePersist);
  }

  private void saveAliasType(List<AliasType> aliasTypePersists){
    log.info("Guardando AliasType");
    List<AliasTypePersist> aliasList = new ArrayList<>();
    for(AliasType at : aliasTypePersists){
      AliasTypePersist aliasTypePersist = AliasTypePersist.builder()
          .aliastype(at.getAliasType())
          .aliastypeId(at.getAliasTypeId())
          .build();

      aliasList.add(aliasTypePersist);
    }

    aliasTypeRepository.saveAll(aliasList);
  }

  private void saveAreaCode(List<AreaCode> areaCodes){
    log.info("Guardando AreaCode");
    List<AreaCodePersist> codeList = new ArrayList<>();
    for(AreaCode ac : areaCodes){
      AreaCodePersist acp = AreaCodePersist.builder()
          .areaCodeid(ac.getAreaCodeId())
          .areacodetype(ac.getAreaCodeId())
          .description(ac.getDescription())
          .areacodename(ac.getAreaCodeName())
          .countryid(ac.getCountryId())
          .build();

      codeList.add(acp);
    }
    areaCodeRepository.saveAll(codeList);
  }

  private void saveCalendar(CalendarType calendarType){
    log.info("Guardando Calendar");
    CalendarPersist cp = CalendarPersist.builder()
        .calendarTypeId(calendarType.getCalendarTypeId())
        .calendarName(calendarType.getCalendarName())
        .build();

    calendarRepository.save(cp);
  }

  private void saveCountry(List<Country> countries, CountryRelevance countryRelevance){
    log.info("Guardando Country");
    List<CountryPersist> countryPersists = new ArrayList<>();
    for(Country cou : countries){
      CountryPersist cop = CountryPersist.builder()
          .countryId(cou.getCountryId())
          .countryName(cou.getCountryName())
          .iso(cou.getIso())
          .build();

      countryPersists.add(cop);
    }
    CountryRelPersist crp = CountryRelPersist.builder()
        .countryRelevanceId(countryRelevance.getCountryRelevanceId())
        .countryRelevanceName(countryRelevance.getCountryRelevanceName())
        .build();

    countryRepository.saveAll(countryPersists);
    countryRelRepository.save(crp);
  }

  private void saveDecisionMaking(DecisionMakingBody makingBody){
    log.info("Guardando DecisionMaking");
    DecisionPersist dp = DecisionPersist.builder()
        .decisionMakingId(makingBody.getDecisionMakingId())
        .organisationId(makingBody.getOrganisationId())
        .decisionMakingName(makingBody.getDecisionMakingName())
        .build();
    decisionRepository.save(dp);
  }

  private void saveDetailRef(List<DetailReference> detailReferences){
    log.info("Guardando DetailReferences");
    List<DetailRefPersist> detailRefPersists = new ArrayList<>();
    for(DetailReference dtr : detailReferences){
      DetailRefPersist drp = DetailRefPersist.builder()
          .detailReferenceId(dtr.getDetailReferenceId())
          .detailReferenceName(dtr.getDetailReferenceName())
          .build();

      detailRefPersists.add(drp);
    }
    detailRefRepository.saveAll(detailRefPersists);
  }

  private void saveDetailType(List<DetailType> detailTypes){
    log.info("Guardando DetailType");
    List<DetailTypePersist> detailTypePersists = new ArrayList<>();
    for(DetailType dt : detailTypes){
      DetailTypePersist dtp = DetailTypePersist.builder()
          .detailTypeId(dt.getDetailTypeId())
          .detailTypeName(dt.getDetailTypeName())
          .build();
      detailTypePersists.add(dtp);
    }
    detailTypeRepository.saveAll(detailTypePersists);
  }

  private void saveDocNameStatus(List<DocNameStatus> docNameStatuses){
    log.info("Guardando DocNameStatus");
    List<DocNamePersist> docNamePersists = new ArrayList<>();
    for(DocNameStatus dns : docNameStatuses){
      DocNamePersist dnp = DocNamePersist.builder()
          .docNameStatusId(dns.getDocNameStatusId())
          .docNameStatusName(dns.getDocNameStatusName())
          .build();
      docNamePersists.add(dnp);
    }
    docNameRepository.saveAll(docNamePersists);
  }

  private void saveEntryEventType(EntryEventType entryEventType){
    log.info("Guardando EntryEvent");
    EntryEventTypePersist eep = EntryEventTypePersist.builder()
        .entryEventTypeId(entryEventType.getEntryEventTypeId())
        .entryEventTypeName(entryEventType.getEntryEventTypeName())
        .build();

    entryEventTypeRepository.save(eep);
  }

  private void saveFeatureType(List<FeatureType> featureTypes){
    log.info("Guardando FeatureType");
    List<FeatureTypePersist> featureTypePersists = new ArrayList<>();
    for (FeatureType ft : featureTypes){
      FeatureTypePersist ftp = FeatureTypePersist.builder()
          .featureGroupId(ft.getFeatureTypeId())
          .featureTypeGroupId(ft.getFeatureTypeGroupID())
          .featureTypeGroupName(ft.getFeatureTypeName())
          .build();

      featureTypePersists.add(ftp);
    }
    featureTypeRepository.saveAll(featureTypePersists);
  }

  private void saveFeatureGroup(FeatureTypeGroup featureTypeGroup){
    log.info("Guardando FeatureGroup");
    FeatureGroupPersist fgp = FeatureGroupPersist.builder()
        .featureGroupId(featureTypeGroup.getFeatureGroupId())
        .featureTypeGroupName(featureTypeGroup.getFeatureTypeGroupName())
        .build();

    featureGroupRepository.save(fgp);
  }

  private void saveRegDocType(List<IDRegDocType> idRegDocTypes){
    log.info("Guardando RegDocType");
    List<RegDocTypePersist> regDocTypePersists = new ArrayList<>();
    for(IDRegDocType rdt : idRegDocTypes){
      RegDocTypePersist rdtp = RegDocTypePersist.builder()
          .iDRegDocTypeId(rdt.getIDRegDocTypeId())
          .iDRegDocTypeName(rdt.getIDRegDocTypeName())
          .build();
      regDocTypePersists.add(rdtp);
    }
    regDocTypeRepository.saveAll(regDocTypePersists);
  }

  private void saveIdentityFeature(IdentityFeatureLinkType ifl){
    log.info("Guardando IdentityFeature");
    IdentityFeaturePersist ifp = IdentityFeaturePersist.builder()
        .identityFeatureLinkTypeId(ifl.getIdentityFeatureLinkTypeId())
        .identityFeatureLinkTypeName(ifl.getIdentityFeatureLinkTypeName())
        .build();
    identityFeatureRepository.save(ifp);
  }

  private void saveLegalBasis(List<LegalBasis> legalBases){
    log.info("Guardando LegalBasis");
    List<LegalBasisPersist> basisPersists = new ArrayList<>();
    for(LegalBasis lb : legalBases){
      LegalBasisPersist lbp = LegalBasisPersist.builder()
          .legalBasisId(lb.getLegalBasisId())
          .legalBasisName(lb.getLegalBasisName())
          .legalBasisShortRef(lb.getLegalBasisShortRef())
          .sanctionsProgramID(lb.getSanctionsProgramID())
          .build();
      basisPersists.add(lbp);
    }
    legalBasisRepository.saveAll(basisPersists);
  }

  private void saveLegalBasisType(LegalBasisType lbt){
    log.info("Guardando LegalBasisType");
    LegalBasisTypePersist lbtp = LegalBasisTypePersist.builder()
        .legalBasisTypeId(lbt.getLegalBasisTypeId())
        .legalBasisTypeName(lbt.getLegalBasisTypeName())
        .build();
    legalBasisTypeRepository.save(lbtp);
  }

  private void saveList(List<ListVal> listValues){
    log.info("Guardando List");
    List<ListPersist> lpl = new ArrayList<>() ;
    for(ListVal lv : listValues){
      ListPersist lp = ListPersist.builder()
          .listId(lv.getListId())
          .listName(lv.getListName())
          .build();
      lpl.add(lp);
    }
    listRepository.saveAll(lpl);
  }

  private void saveLocPartType(List<LocPartType> locPartTypes){
    log.info("Guardando LocPartType");
    List<LocPartPersist> locPartPersists = new ArrayList<>();
    for(LocPartType lpt : locPartTypes){
      LocPartPersist lptp = LocPartPersist.builder()
          .locPartTypeId(lpt.getLocPartTypeId())
          .locPartTypeName(lpt.getLocPartTypeName())
          .build();
      locPartPersists.add(lptp);
    }
    locPartRepository.saveAll(locPartPersists);
  }

  private void saveLocPartStatus(LocPartValueStatus locPartValueStatus){
    log.info("Guardando LocPartStatus");
    LocPartValueStatusPersist locPartValueStatusPersist = LocPartValueStatusPersist.builder()
        .locPartValueStatusId(locPartValueStatus.getLocPartValueStatusId())
        .locPartValueStatusName(locPartValueStatus.getLocPartValueStatusName())
        .build();

    locPartValueStatusRepository.save(locPartValueStatusPersist);
  }

  private void saveLocPartValType(LocPartValueType locPartValueType){
    log.info("Guardando LocPartValType");
    LocPartValueTypePersist locPartValueTypePersist = LocPartValueTypePersist.builder()
        .locPartValueTypeId(locPartValueType.getLocPartValueTypeId())
        .locPartValueTypeName(locPartValueType.getLocPartValueTypeName())
        .build();

    locPartValueTypeRepository.save(locPartValueTypePersist);
  }

  private void saveNamePartType(List<NamePartType> namePartTypes){
    log.info("Guardando NamePartType");
    List<NamePartTypePersist> namePartPersists = new ArrayList<>();
    for(NamePartType npt : namePartTypes){
      NamePartTypePersist npp = NamePartTypePersist.builder()
          .namePartTypeId(npt.getNamePartTypeId())
          .namePartTypeName(npt.getNamePartTypeName())
          .build();
      namePartPersists.add(npp);
    }
    namePartTypeRepository.saveAll(namePartPersists);
  }

  private void saveOrg(Organisation org){
    log.info("Guardando Organisation");
    OrgPersist orgPersist = OrgPersist.builder()
        .organisationId(org.getOrganisationId())
        .organisationName(org.getOrganisationName())
        .countryId(org.getCountryId())
        .build();

    orgRepo.save(orgPersist);
  }

  private void savePartSubType(List<PartySubType> partySubTypes){
    log.info("Guardando PartSubType");
    List<PartySubtypePersist> subtypePersists = new ArrayList<>();
    partySubTypes.stream().map(this::getPartySubtypePersist).parallel().forEach(partySubtypeRepository::save);
  }


  private PartySubtypePersist getPartySubtypePersist(PartySubType pst) {
    PartySubtypePersist pstp = PartySubtypePersist.builder()
        .partySubTypeId(pst.getPartySubTypeId())
        .partySubTypeName(pst.getPartySubTypeName())
        .partyTypeID(pst.getPartyTypeID())
        .build();
    return pstp;
  }

  private void savePartyType(List<PartyType> partyTypes){
    log.info("Guardando PartyType");
    List<PartyTypePersist> partyTypePersists = new ArrayList<>();
    for(PartyType pt : partyTypes){
      PartyTypePersist ptp = PartyTypePersist.builder()
          .partyTypeId(pt.getPartyTypeId())
          .partyTypeName(pt.getPartyTypeName())
          .build();
      partyTypePersists.add(ptp);
    }
    partyTypeRepository.saveAll(partyTypePersists);
  }

  private void saveRelQuality(List<RelationQuality> rqs){
    log.info("Guardando RelationQuality");
    List<RelQualityPersist> rqps = new ArrayList<>();
    for(RelationQuality rq : rqs){
      RelQualityPersist rqp = RelQualityPersist.builder()
          .relationQualityId(rq.getRelationQualityId())
          .relationQualityName(rq.getRelationQualityName())
          .build();
      rqps.add(rqp);
    }
    relQualityRepository.saveAll(rqps);
  }

  private void saveRelationType(List<RelationType> relationTypes){
    log.info("Guardando RelationType");
    List<RelTypePersist> relTypePersists = new ArrayList<>();
    for(RelationType rt : relationTypes){
      RelTypePersist rtp = RelTypePersist.builder()
          .relationTypeId(rt.getRelationTypeId())
          .relationTypeName(rt.getRelationTypeName())
          .symmetrical(rt.isSymmetrical())
          .build();
      relTypePersists.add(rtp);
    }
    relTypeRepository.saveAll(relTypePersists);
  }

  private void saveReliability(List<Reliability> reliabilities){
    log.info("Guardando Reliability");
    List<ReliabilityPersist> reliabilityPersists = new ArrayList<>();
    for(Reliability rel : reliabilities){
      ReliabilityPersist relp = ReliabilityPersist.builder()
          .reliabilityId(rel.getReliabilityId())
          .reliabilityName(rel.getReliabilityName())
          .build();
      reliabilityPersists.add(relp);
    }
    reliabilityRepository.saveAll(reliabilityPersists);
  }

  private void saveSanctionProg(List<SanctionsProgram> sanctionsPrograms){
    log.info("Guardando SanctionProgram");
    List<SanctionProgPersist> sanctionProgPersists = new ArrayList<>();
    for(SanctionsProgram sp : sanctionsPrograms){
      SanctionProgPersist spp = SanctionProgPersist.builder()
          .sanctionProgramId(sp.getSanctionProgramId())
          .subsidiaryId(sp.getSubsidiaryId())
          .sanctionProgramName(sp.getSanctionProgramName())
          .build();
      sanctionProgPersists.add(spp);
    }
    sanctionProgRepository.saveAll(sanctionProgPersists);
  }

  private void saveSanctionType(List<SanctionsType> sanctionsTypes){
    log.info("Guardando SanctionType");
    List<SanctionTypePersist> sanctionTypePersists = new ArrayList<>();
    for(SanctionsType sct : sanctionsTypes){
      SanctionTypePersist sctp = SanctionTypePersist.builder()
          .sanctionsTypeId(sct.getSanctionsTypeId())
          .sanctionsTypeName(sct.getSanctionsTypeName())
          .build();
      sanctionTypePersists.add(sctp);
    }
    sanctionTypeRepository.saveAll(sanctionTypePersists);
  }

  private void saveScript(List<Script> scripts){
    log.info("Guardando Script");
    List<ScriptPersist> scriptPersists = new ArrayList<>();
    for(Script sc : scripts){
      ScriptPersist scp = ScriptPersist.builder()
          .scriptId(sc.getScriptId())
          .scriptCode(sc.getScriptCode())
          .scriptName(sc.getScriptName())
          .build();
      scriptPersists.add(scp);
    }
    scriptRepository.saveAll(scriptPersists);
  }

  private void saveScriptStatus(ScriptStatus scriptStatus){
    log.info("Guardando ScriptStatus");
    ScriptStatusPersist scst = ScriptStatusPersist.builder()
        .scriptStatusId(scriptStatus.getScriptStatusId())
        .scriptStatusName(scriptStatus.getScriptStatusName())
        .build();
    scriptStatusRepository.save(scst);
  }

  private void saveSubsidiary(SubsidiaryBody subsidiaryBody){
    log.info("Guardando Subsidiary");
    SubsidiaryPersist subp = SubsidiaryPersist.builder()
        .subsidiaryBodyId(subsidiaryBody.getSubsidiaryBodyId())
        .notional(subsidiaryBody.isNotional())
        .decisionMakingBodyID(subsidiaryBody.getDecisionMakingBodyID())
        .subsidiaryBodyName(subsidiaryBody.getSubsidiaryBodyName())
        .build();
    subsidiaryRepository.save(subp);
  }

  private void saveValidity(List<Validity> validities){
    log.info("Guardando Validity");
    List<ValidityPersist> validityPersists = new ArrayList<>();
    for(Validity v : validities) {
      ValidityPersist vp = ValidityPersist.builder()
          .validityId(v.getValidityId())
          .validityName(v.getValidityName())
          .build();
      validityPersists.add(vp);
    }
    validityRepository.saveAll(validityPersists);
  }

  // FIN REFERENCES VALUES

  private void saveLocation(List<Location> locations){
    log.info("Guardando Location");
    locations.stream().peek(this::hasLocationPart).map(this::getLocationPersist).parallel().forEach(locationRepository::save);
  }

  private void hasLocationPart(Location l) {
    if(l.getLocationPart() != null) {
      saveLocationPart(l.getLocationPart(), l.getLocationId());
    }
  }

  private LocationPersist getLocationPersist(Location l) {
    LocationPersist locp = LocationPersist.builder()
        .locationId(l.getLocationId())
        .areaCodeID(l.getAreaCode()==null ? 0 : l.getAreaCode().getAreaCodeID())
        .countryId(l.getCountry()==null ? 0 : l.getCountry().getCountryId())
        .countryRelevance(l.getCountry()==null ? 0 : l.getCountry().getCountryRelevance())
        .build();
    return locp;
  }

  private void saveLocationPart(List<LocationPart> locationParts, int locationId){
    log.info("Guardando LocationPart");
    locationParts.stream().map(locationPart ->
       getLocationPartPersist(locationId, locationPart)
    ).parallel().forEach(locationPartRepository::save);
  }

  private LocationPartPersist getLocationPartPersist(int locationId, LocationPart locationPart) {
    return LocationPartPersist.builder()
                .locPartTypeId(locationPart.getLocPartTypeId())
                .locationId(locationId)
                .nameLocation(locationPart.getPartValue().getNameLocation())
                .build();
  }

  private void saveRegDocuments(List<RegDocument> regDocuments){
    log.info("Guardando RegDocuments");
    regDocuments.stream().map(this::getRegDocumentsPersist).parallel().forEach(regDocumentsRepository::save);
  }

  private RegDocumentsPersist getRegDocumentsPersist(RegDocument reg) {
    RegDocumentsPersist rgdp = RegDocumentsPersist.builder()
        .regDocumentId(reg.getRegDocumentId())
        .docType(reg.getDocType())
        .regDocumentIdentity(reg.getIdentity())
        .issuedBy(reg.getIssuedBy())
        .validity(reg.getValidity())
        .registrationNumber(reg.getRegistrationNumber())
        .nameId(reg.getNameReference().getNameId())
        .build();
    return rgdp;
  }

  private void saveProfileRelation(List<ProfileRelationship> profileRelationships){
    log.info("Guardando ProfileRelation");
    profileRelationships.stream().map(this::getProfilePersist).parallel().forEach(profileRepository::save);
  }

  private ProfilePersist getProfilePersist(ProfileRelationship prel) {
    ProfilePersist prop = ProfilePersist.builder()
        .relationshipID(prel.getRelationshipID())
        .fromId(prel.getFromId())
        .toId(prel.getToId())
        .typeId(prel.getTypeId())
        .qualityId(prel.getQualityId())
        .former(prel.isFormer())
        .sanctionId(prel.getSanctionId())
        .build();
    return prop;
  }

  private void saveDistinctParty(List<DistinctParty> distinctParties){
    log.info("Guardando DistinctParty");
    distinctParties.stream().parallel()
            .peek(dp ->saveProfile(dp.getProfile(), dp.getFixedRef()))
            .peek(dp -> saveIdentity(dp.getProfile().getIdentity(),dp.getProfile().getProfileId()))
            .peek(dp -> saveAlias(dp.getProfile().getIdentity().getAlias(),dp.getProfile().getIdentity().getIdentityId()))
            .map(this::getDistinctPartyPersist).forEach(distinctPartyRepository::save);
  }

  private DistinctPartyPersist getDistinctPartyPersist(DistinctParty dp) {
    DistinctPartyPersist dpp = DistinctPartyPersist.builder()
        .fixedRef(dp.getFixedRef())
        .profileId(dp.getProfile().getProfileId())
        .build();
    return dpp;
  }

  private void saveProfile(Profile profile, int distinctPartyId){
    log.info("Guardando Profile");
    ProfilePartyPersist prpp = ProfilePartyPersist.builder()
        .profileId(profile.getProfileId())
        .partySubType(profile.getPartySubType())
        .distinctPartyId(distinctPartyId)
        .build();
    profilePartyRepository.save(prpp);
  }

  private void saveIdentity(Identity identity, int profileId){
    log.info("Guardando Identity");
    IdentityPersist ip = IdentityPersist.builder()
        .identityId(identity.getIdentityId())
        .profileId(profileId)
        .identityPrimary(identity.isPrimary())
        .falseValue(identity.isFalseValue())
        .build();
    identityRepository.save(ip);

    saveNamePartGroups(identity.getNamePartGroups().getMasterNamePartGroups(),identity.getIdentityId());

  }

  private void saveNamePartGroups(List<MasterNamePartGroup> masterNamePartGroups,int identityId){
    for(MasterNamePartGroup partGroup : masterNamePartGroups){
      for(NamePartGroup namePartGroup : partGroup.getNamePartGroups()){
        MasterNamePartGroupPersist groupPersist = MasterNamePartGroupPersist.builder()
            .namePartGroupId(namePartGroup.getNamePartGroupId())
            .namePartTypeId(namePartGroup.getNamePartTypeId())
            .identityID(identityId)
            .build();
        masterNamePartGroupRepository.save(groupPersist);
      }
    }

  }

  private void saveAlias(List<Alias> aliasList, int identityId){
    log.info("Guardando Alias");
    for(Alias al : aliasList){
      AliasPersist alp = AliasPersist.builder()
          .fixedRef(al.getFixedRef())
          .documentNameId(al.getDocumentedName().getDocumentID())
          .identityId(identityId)
          .aliasType(al.getAliasType())
          .aliasPrimary(al.isPrimary())
          .lowQuality(al.isLowQuality())
          .build();

      aliasRepository.save(alp);
      saveDocumentedName(al.getDocumentedName(),al.getFixedRef());
    }
  }

  private void saveDocumentedName(DocumentedName dn, int alId){
    log.info("Guardando DocumentedName");
    DocumentedNamePersist dnp = DocumentedNamePersist.builder()
        .documentID(dn.getDocumentID())
        .docFixedRef(dn.getDocFixedRef())
        .docNameStatus(dn.getDocNameStatus())
        .aliasId(alId)
        .build();

    documentedNameRepository.save(dnp);

    for(DocumentedNamePart np : dn.getDocumentedNameParts()){
      saveNamePart(np.getNamePartValues(),dn.getDocumentID());
    }

  }

  private void saveNamePart(List<NamePartValue> namePartValues, int dni){
    log.info("Guardando NamePart");
    namePartValues.stream().map(dnpart -> getNamePartPersist(dni, dnpart)).parallel().forEach(namePartRepository::save);
  }

  private NamePartPersist getNamePartPersist(int dni, NamePartValue dnpart) {
    NamePartPersist npp = NamePartPersist.builder()
        .partGroup(dnpart.getPartGroup())
        .script(dnpart.getScript())
        .scriptStatus(dnpart.getScriptStatus())
        .acronym(dnpart.isAcronym())
        .value(dnpart.getValue())
        .documentNameId(dni)
        .documentNameId(dni)
        .build();
    return npp;
  }

  private void saveSanctionEntry(List<SanctionEntry> sanctionEntries){
    log.info("Guardando SanctionEntry");
    sanctionEntries.stream()
            .peek(se -> saveEntryEvent(se.getEntryEvents()))
            .peek(se ->  saveEntryMeasure(se.getMeasure()))
            .map(this::getSanctionEntryPersist).parallel().forEach(sanctionEntryRepository::save);
  }

  private SanctionEntryPersist getSanctionEntryPersist(SanctionEntry se) {
    SanctionEntryPersist sep = SanctionEntryPersist.builder()
        .entryId(se.getEntryId())
        .profileId(se.getProfileId())
        .listId(se.getListId())
        .build();
    return sep;
  }

  private void saveEntryEvent(EntryEvent entryEvent){
    log.info("Guardando EntryEvent");
    EntryEventPersist eep = EntryEventPersist.builder()
        .entryId(entryEvent.getEntryId())
        .eventType(entryEvent.getEventType())
        .legalBasis(entryEvent.getLegalBasis())
        .calendarType(entryEvent.getEntryDate().getCalendarType())
        .year(entryEvent.getEntryDate().getMonth())
        .month(entryEvent.getEntryDate().getMonth())
        .day(entryEvent.getEntryDate().getDay())
        .build();
    entryEventRepository.save(eep);

  }

  private void saveEntryMeasure(List<Measure> measures){
    log.info("Guardando EntryMeasure");
    measures.stream().map(this::getMeasurePersist).parallel().forEach(measureRepository::save);
  }

  private MeasurePersist getMeasurePersist(Measure mes) {
    return MeasurePersist.builder()
        .measureId(mes.getMeasureId())
        .calendarTypeID(mes.getDatePeriod().getCalendarTypeID())
        .sanctionTypeId(mes.getTypeId())
        .yearFixed(mes.getDatePeriod().isYearFixed())
        .monthFixed(mes.getDatePeriod().isMonthFixed())
        .dayFixed(mes.getDatePeriod().isDayFixed())
        .comment(mes.getComment())
        .build();
  }


}
