package com.data.ofac.repository;

import com.data.ofac.domain.IdentityPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IdentityRepository extends CrudRepository<IdentityPersist, Long> {

}
