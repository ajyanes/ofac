package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.CountryRelPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRelRepository extends CrudRepository<CountryRelPersist,Long> {

}
