package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.LegalBasisTypePersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LegalBasisTypeRepository extends CrudRepository<LegalBasisTypePersist,Long> {

}
