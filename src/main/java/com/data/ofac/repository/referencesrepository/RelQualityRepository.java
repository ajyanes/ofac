package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.RelQualityPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RelQualityRepository extends CrudRepository<RelQualityPersist,Long> {

}
