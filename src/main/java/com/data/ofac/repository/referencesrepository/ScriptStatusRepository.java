package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.ScriptStatusPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScriptStatusRepository extends CrudRepository<ScriptStatusPersist,Long> {

}
