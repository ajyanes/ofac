package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.PartySubtypePersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PartySubtypeRepository extends CrudRepository<PartySubtypePersist,Long> {

}
