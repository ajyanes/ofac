package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.DetailRefPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DetailRefRepository extends CrudRepository<DetailRefPersist,Long> {

}
