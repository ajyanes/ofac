package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.PartyTypePersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PartyTypeRepository extends CrudRepository<PartyTypePersist,Long> {

}
