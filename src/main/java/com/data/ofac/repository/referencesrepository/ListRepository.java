package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.ListPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ListRepository extends CrudRepository<ListPersist,Long> {

}
