package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.AreaCodePersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AreaCodeTypeRepository extends CrudRepository<AreaCodePersist,Long> {

}
