package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.NamePartTypePersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NamePartTypeRepository extends CrudRepository<NamePartTypePersist,Long> {

}
