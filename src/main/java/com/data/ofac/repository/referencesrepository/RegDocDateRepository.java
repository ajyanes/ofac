package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.RegDocDatePersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RegDocDateRepository extends CrudRepository<RegDocDatePersist,Long> {

}
