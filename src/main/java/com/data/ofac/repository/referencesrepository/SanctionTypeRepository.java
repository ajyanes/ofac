package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.SanctionTypePersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SanctionTypeRepository extends CrudRepository<SanctionTypePersist,Long> {

}
