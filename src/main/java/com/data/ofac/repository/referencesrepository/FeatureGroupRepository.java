package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.FeatureGroupPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FeatureGroupRepository extends CrudRepository<FeatureGroupPersist,Long> {

}
