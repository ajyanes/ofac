package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.FeatureTypePersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FeatureTypeRepository extends CrudRepository<FeatureTypePersist,Long> {

}
