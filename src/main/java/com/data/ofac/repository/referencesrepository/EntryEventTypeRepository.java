package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.EntryEventTypePersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EntryEventTypeRepository extends CrudRepository<EntryEventTypePersist,Long> {

}
