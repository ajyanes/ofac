package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.LocPartValueTypePersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocPartValueTypeRepository extends CrudRepository<LocPartValueTypePersist,Long> {

}
