package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.AliasTypePersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AliasTypeRepository extends CrudRepository<AliasTypePersist,Long> {

}
