package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.DocNamePersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocNameRepository extends CrudRepository<DocNamePersist,Long> {

}
