package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.IdentityFeaturePersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IdentityFeatureRepository extends CrudRepository<IdentityFeaturePersist,Long> {

}
