package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.SanctionProgPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SanctionProgRepository extends CrudRepository<SanctionProgPersist,Long> {

}
