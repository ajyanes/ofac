package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.RegDocTypePersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RegDocTypeRepository extends CrudRepository<RegDocTypePersist,Long> {

}
