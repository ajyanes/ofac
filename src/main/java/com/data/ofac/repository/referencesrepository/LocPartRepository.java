package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.LocPartPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocPartRepository extends CrudRepository<LocPartPersist,Long> {

}
