package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.ScriptPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScriptRepository extends CrudRepository<ScriptPersist,Long> {

}
