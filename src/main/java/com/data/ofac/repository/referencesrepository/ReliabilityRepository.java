package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.ReliabilityPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReliabilityRepository extends CrudRepository<ReliabilityPersist,Long> {

}
