package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.CountryPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends CrudRepository<CountryPersist,Long> {

}
