package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.ValidityPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ValidityRepository extends CrudRepository<ValidityPersist,Long> {

}
