package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.DetailTypePersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DetailTypeRepository extends CrudRepository<DetailTypePersist,Long> {

}
