package com.data.ofac.repository.referencesrepository;

import com.data.ofac.domain.subdomain.DecisionPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DecisionRepository extends CrudRepository<DecisionPersist,Long> {

}
