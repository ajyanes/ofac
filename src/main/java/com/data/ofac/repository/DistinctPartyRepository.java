package com.data.ofac.repository;

import com.data.ofac.domain.DistinctPartyPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DistinctPartyRepository extends CrudRepository<DistinctPartyPersist, Long> {

}
