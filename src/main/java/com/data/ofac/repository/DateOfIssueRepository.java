package com.data.ofac.repository;

import com.data.ofac.domain.DateOfIssuePersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DateOfIssueRepository extends CrudRepository<DateOfIssuePersist, Long> {

}
