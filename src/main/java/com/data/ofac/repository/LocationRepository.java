package com.data.ofac.repository;

import com.data.ofac.domain.LocationPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRepository extends CrudRepository<LocationPersist, Long> {

}
