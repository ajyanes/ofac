package com.data.ofac.repository;

import com.data.ofac.domain.AliasPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AliasRepository extends CrudRepository<AliasPersist, Long> {

}
