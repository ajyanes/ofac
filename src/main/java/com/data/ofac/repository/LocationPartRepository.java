package com.data.ofac.repository;

import com.data.ofac.domain.LocationPartPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationPartRepository extends CrudRepository<LocationPartPersist, Long> {

}
