package com.data.ofac.repository;

import com.data.ofac.domain.MasterNamePartGroupPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MasterNamePartGroupRepository extends CrudRepository<MasterNamePartGroupPersist, Long> {

}
