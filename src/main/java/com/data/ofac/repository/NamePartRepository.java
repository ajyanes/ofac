package com.data.ofac.repository;

import com.data.ofac.domain.NamePartPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NamePartRepository  extends CrudRepository<NamePartPersist, Long> {

}
