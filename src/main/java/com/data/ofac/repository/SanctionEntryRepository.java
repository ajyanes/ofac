package com.data.ofac.repository;

import com.data.ofac.domain.SanctionEntryPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SanctionEntryRepository extends CrudRepository<SanctionEntryPersist, Long> {

}
