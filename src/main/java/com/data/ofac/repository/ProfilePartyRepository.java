package com.data.ofac.repository;

import com.data.ofac.domain.ProfilePartyPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfilePartyRepository extends CrudRepository<ProfilePartyPersist, Long> {

}
