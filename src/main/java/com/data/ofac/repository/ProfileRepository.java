package com.data.ofac.repository;

import com.data.ofac.domain.ProfilePersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfileRepository extends CrudRepository<ProfilePersist, Long> {

}
