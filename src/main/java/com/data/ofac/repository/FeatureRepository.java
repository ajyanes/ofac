package com.data.ofac.repository;

import com.data.ofac.domain.FeaturePartyPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FeatureRepository  extends CrudRepository<FeaturePartyPersist, Long> {

}
