package com.data.ofac.repository;

import com.data.ofac.domain.RegDocumentsPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RegDocumentsRepository extends CrudRepository<RegDocumentsPersist, Long> {

}
