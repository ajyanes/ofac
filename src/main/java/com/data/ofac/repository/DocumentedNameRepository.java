package com.data.ofac.repository;

import com.data.ofac.domain.DocumentedNamePersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentedNameRepository extends CrudRepository<DocumentedNamePersist, Long> {

}
