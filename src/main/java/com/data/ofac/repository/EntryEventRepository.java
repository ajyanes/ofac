package com.data.ofac.repository;

import com.data.ofac.domain.EntryEventPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EntryEventRepository extends CrudRepository<EntryEventPersist, Long> {

}
