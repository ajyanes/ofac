package com.data.ofac.repository;

import com.data.ofac.domain.MeasurePersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MeasureRepository extends CrudRepository<MeasurePersist, Long> {

}
