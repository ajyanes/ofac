package com.data.ofac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OfacApplication {

  public static void main(String[] args) {
    SpringApplication.run(OfacApplication.class, args);
  }

}
