package com.data.ofac.task;

import com.data.ofac.service.OfacService;
import java.io.IOException;
import java.net.HttpURLConnection;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Config {

  private Integer code;

  @Autowired
  private OfacService ofacService;

  @PostConstruct
  void startProcess(){
    log.info("Validando URL");
    try{
      code = ofacService.validateUrl();
    } catch (IOException e) {
      log.info("ERROR "+e.getMessage());
      code = 0;
    }

    if(code == HttpURLConnection.HTTP_OK) {
      log.info("URL disponible");
      ofacService.saveOfac();
    }else{
      log.info("URL NO DISPONIBLE. INTENTE MAS TARDE!!");
    }

  }

}
