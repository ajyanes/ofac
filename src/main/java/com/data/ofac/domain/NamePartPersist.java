package com.data.ofac.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ofac_namepart")
public class NamePartPersist implements Serializable {

  @Id
  private int partGroup;
  private int script;
  private int scriptStatus;
  private boolean acronym;
  private String value;
  private int documentNameId;

}
