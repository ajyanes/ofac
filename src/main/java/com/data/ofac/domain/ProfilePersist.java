package com.data.ofac.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ofac_profile")
public class ProfilePersist implements Serializable {

  @Id
  private int relationshipID;
  private int fromId;
  private int toId;
  private int typeId;
  private int qualityId;
  private boolean former;
  private int sanctionId;

}
