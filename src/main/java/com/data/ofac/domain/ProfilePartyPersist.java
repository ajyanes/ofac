package com.data.ofac.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ofac_profileparty")
public class ProfilePartyPersist implements Serializable {

  @Id
  private int profileId;
  private int partySubType;
  private int distinctPartyId;

}
