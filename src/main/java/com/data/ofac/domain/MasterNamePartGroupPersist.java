package com.data.ofac.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ofac_masternamepartgroup")
public class MasterNamePartGroupPersist implements Serializable {

  @Id
  private int namePartGroupId=0;
  private int namePartTypeId=0;
  private int identityID;

}
