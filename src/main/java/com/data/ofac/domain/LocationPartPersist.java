package com.data.ofac.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ofac_locationpart")
@IdClass(LocationPartPk.class)
public class LocationPartPersist implements Serializable {

  @Id
  private int locPartTypeId;
  @Id
  private int locationId;
  private String nameLocation;

}
