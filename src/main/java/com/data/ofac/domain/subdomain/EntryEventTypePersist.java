package com.data.ofac.domain.subdomain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ofac_entryeventtype")
public class EntryEventTypePersist implements Serializable {

  @Id
  private int entryEventTypeId;
  private String entryEventTypeName;

}
