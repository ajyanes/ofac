package com.data.ofac.domain.subdomain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ofac_legalbasis")
public class LegalBasisPersist implements Serializable {

  @Id
  private int legalBasisId;
  private String legalBasisShortRef;
  private int legalBasisTypeID;
  private int sanctionsProgramID;
  private String legalBasisName;

}
