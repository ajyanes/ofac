package com.data.ofac.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ofac_location")
public class LocationPersist implements Serializable {

  @Id
  private int locationId;
  private int areaCodeID;
  private int countryId;
  private int countryRelevance;

}
