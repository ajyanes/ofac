package com.data.ofac.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ofac_documentedname")
public class DocumentedNamePersist implements Serializable {

  @Id
  private int documentID;
  private int docFixedRef;
  private int docNameStatus;
  private int aliasId;

}
