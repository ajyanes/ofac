package com.data.ofac.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ofac_identity")
public class IdentityPersist implements Serializable {

  @Id
  private int identityId;
  private int profileId;
  private int fixedRef;
  private boolean identityPrimary;
  private boolean falseValue;

}
