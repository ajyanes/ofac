package com.data.ofac.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ofac_regdocument")
public class RegDocumentsPersist {

  @Id
  private int regDocumentId;
  private int docType;
  private int regDocumentIdentity;
  private int issuedBy;
  private int validity;
  private String registrationNumber;
  private int nameId;

}
