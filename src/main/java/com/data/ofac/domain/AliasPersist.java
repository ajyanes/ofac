package com.data.ofac.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ofac_alias")
@IdClass(AliasPersistPk.class)
public class AliasPersist implements Serializable {

  @Id
  private int fixedRef;
  @Id
  private int documentNameId;
  private int identityId;
  private int aliasType;
  private boolean aliasPrimary;
  private boolean lowQuality;

}
