package com.data.ofac.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ofac_entrymeasure")
public class MeasurePersist implements Serializable {

  @Id
  private int measureId;
  private int sanctionTypeId;
  private int calendarTypeID;
  private boolean yearFixed;
  private boolean monthFixed;
  private boolean dayFixed;
  private String comment;

}
