package com.data.ofac.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ofac_entryevent")
public class EntryEventPersist implements Serializable {

  @Id
  private int entryId;
  private int eventType;
  private int legalBasis;
  private int calendarType;
  private String year;
  private String month;
  private String day;

}
